package personal.rsa.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import personal.rsa.game.Action;
import personal.rsa.game.GameState;


public class RandomAIPlayer extends Player {

  public RandomAIPlayer(int playerId) {
    super(playerId);
  }

  @Override
  public Action doAction(List<String> myCards, int myCoins) {
    // get other players
    List<Integer> otherPlayers = GameState.getOtherPlayers(playerId);

    Random ranPlayer = new Random();
    int size = otherPlayers.size();
    int val = ranPlayer.nextInt(size);
    int targetPlayer = otherPlayers.get(val);

    if (myCoins >= 10) {
      return new Action(playerId, Action.COUP, targetPlayer);
    }

    while (true) {
      Random ran = new Random();
      int x = ran.nextInt(7);
      if (x == 0) return new Action(playerId, Action.TAKE_INCOME);
      else if (x == 1) return new Action(playerId, Action.TAKE_FOREIGN_AID);
      else if (x == 2) return new Action(playerId, Action.TAKE_3_AS_DUKE);
      else if (x == 3) return new Action(playerId, Action.STEAL_2_AS_CAPTAIN, targetPlayer);
      else if (x == 4) return new Action(playerId, Action.EXCHANGE_AS_AMBASSADOR);
      else if (x == 5) {
        if (myCoins >= 3) {
          return new Action(playerId, Action.ASSASSINATE, targetPlayer);
        } else { continue; }
      }
      else { // x == 6
        if (myCoins >= 7) {
          return new Action(playerId, Action.COUP, targetPlayer);
        } else { continue; }
      }
    }
  }

  @Override
  public Action doCounterAction(List<String> myCards, int myCoins, Action actionAgainstMe) {
    int playerDoingActionAgainstMe = actionAgainstMe.playerDoingAction;
    String actionAgainstMeValue = actionAgainstMe.action;

    if (actionAgainstMeValue == Action.STEAL_2_AS_CAPTAIN) {
      Random ran = new Random();
      int x = ran.nextInt(3);
      if (x == 0) return new Action(playerId, Action.BLOCK_STEAL_AS_CAPTAIN);
      else if (x == 1) return new Action(playerId, Action.BLOCK_STEAL_AS_AMBASSADOR);
      else return null;
    } else if (actionAgainstMeValue == Action.ASSASSINATE) {
      Random ran = new Random();
      int x = ran.nextInt(2);
      if (x == 0) return new Action(playerId, Action.BLOCK_ASSASSINATION);
      else if (x == 1) return null;
    } else { // foreign aid
      Random ran = new Random();
      int x = ran.nextInt(2);
      if (x == 0) return new Action(playerId, Action.BLOCK_FOREIGN_AID);
      else if (x == 1) return null;
    }
    return null; // unreachable
  }

  @Override
  public boolean doBS(List<String> myCards, int myCoins, Action action) {
    Random ran = new Random();
    int x = ran.nextInt(2);
    if (x == 0) return true;
    return false;
  }

  @Override
  public List<String> resolveAmbassador(List<String> myCards, int myCoins, List<String> newCards) {
    List<String> retval = new ArrayList<String>();
    List<String> allCards = new ArrayList<String>();
    allCards.addAll(myCards);
    allCards.addAll(newCards);

    if (allCards.size() == 4) {
      Random ran = new Random();
      int x = ran.nextInt(4);
      retval.add(allCards.remove(x));
      int y = ran.nextInt(3);
      retval.add(allCards.remove(y));
    } else { // size = 3
      Random ran = new Random();
      int x = ran.nextInt(3);
      retval.add(allCards.remove(x));
      int y = ran.nextInt(2);
      retval.add(allCards.remove(y));
    }
    return retval;
  }

  @Override
  public String loseCard(List<String> myCards, int myCoins) {
    Random ran = new Random();
    int x = ran.nextInt(2);
    if (x == 0) return myCards.get(0);
    return myCards.get(1);
  }
}
