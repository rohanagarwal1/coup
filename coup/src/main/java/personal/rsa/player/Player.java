package personal.rsa.player;

import java.util.List;
import personal.rsa.game.Action;

// a player should start knowing what the GameState is.
// the player should also know their cards and how many coins they have. Why though.. why not let them fetch from GameState?

// in real life, you pick up your cards, that's like accessing the GameState. You can't pick up someone else's cards though.
// same way, the Player should be able to access their cards from GameState, but be isolated from someone else's cards.

// give the player a token, which GameState will map to their identity. The person can then pass in the token
// in order to get their cards.

public abstract class Player {

  public int playerId;

  public Player(int playerId) {
    this.playerId = playerId;
  }

  // stable API, return a valid action
  public abstract Action doAction(List<String> myCards, int myCoins);

  // choose to do a counter action. This signature will probably change to have an arg with valid choices for counteraction
  public abstract Action doCounterAction(List<String> myCards, int myCoins, Action actionAgainstMe);

  // this signature will probably change to have an argument showing what last action or counteraction was.
  public abstract boolean doBS(List<String> myCards, int myCoins, Action action);

  // stable API, return the cards you don't want to keep
  public abstract List<String> resolveAmbassador(List<String> myCards, int myCoins, List<String> newCards);

  // stable API, return the card you want to lose
  public abstract String loseCard(List<String> myCards, int myCoins);

}
