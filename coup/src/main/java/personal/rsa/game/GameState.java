package personal.rsa.game;
import java.util.*;
import personal.rsa.player.Player;
import personal.rsa.player.RandomAIPlayer;


public class GameState {

  // internal state of the deck
  private static List<String> deck = new ArrayList<String>();

  // these players are the GameStates internal knowledge of what cards they have, and how many coins
  private static List<PlayerState> players = new ArrayList<PlayerState>();

  public static List<String> revealedCards = new ArrayList<String>();

  // these AIPlayers are the actual players
  private static List<Player> AIs = new ArrayList<Player>();

  public static void main(String [] args) throws Exception {
    System.out.println("Hi there! Let's play Coup!\nLet's start by shuffling the deck");
    System.out.println("How many players? Assuming 3 for now");

    constructDeck();
    initializePlayers();
    playGame();
  }

  private static void constructDeck(){
    deck = new ArrayList<String>();
    deck.add(Card.DUKE);
    deck.add(Card.DUKE);
    deck.add(Card.DUKE);
    deck.add(Card.AMBASSADOR);
    deck.add(Card.AMBASSADOR);
    deck.add(Card.AMBASSADOR);
    deck.add(Card.CONTESSA);
    deck.add(Card.CONTESSA);
    deck.add(Card.CONTESSA);
    deck.add(Card.CAPTAIN);
    deck.add(Card.CAPTAIN);
    deck.add(Card.CAPTAIN);
    deck.add(Card.ASSASSIN);
    deck.add(Card.ASSASSIN);
    deck.add(Card.ASSASSIN);
    Collections.shuffle(deck);
    System.out.println(deck);
  }

  private static void initializePlayers() {
    AIs = new ArrayList<Player>();
    players = new ArrayList<PlayerState>();

    for (int i = 0; i < 3; i++) {
      // get cards for player
      List<String> playerCards = getStartingCardsForPlayer();

      AIs.add(new RandomAIPlayer(i));
      players.add(new PlayerState(playerCards, i));
    }
  }
  private static List<String> getStartingCardsForPlayer() {
    List<String> cards = new ArrayList<String>();
    cards.add(deck.remove(0));
    cards.add(deck.remove(0));
    return cards;
  }

  private static void playGame() throws Exception {
    logPlayerStates();
    while (true) {
      for (Player player : AIs) {
        // log the current state of game

        // end the game if only one player left
        if (checkIfGameIsDone()) {
          // log winner
          logWinner();
          System.out.println("Thanks for playing!");
          System.exit(0);
        }

        // check if this player has cards left and is still playing
        if (getPlayerStateForId(player.playerId).cards.size() == 0) {
          continue;
        }

        // get player's cards and coins
        PlayerState playerState = getPlayerStateForId(player.playerId);

        // get the player's action
        Action playerAction = player.doAction(playerState.cards, playerState.coins);
        validateAction(playerState, playerAction);
        logAction(playerAction);
        if (playerAction.action == Action.TAKE_INCOME) playIncomeRound(playerState, playerAction);
        else if (playerAction.action == Action.TAKE_FOREIGN_AID) playForeignAidRound(playerState, playerAction);
        else if (playerAction.action == Action.TAKE_3_AS_DUKE) playDukeRound(player, playerState, playerAction);
        else if (playerAction.action == Action.STEAL_2_AS_CAPTAIN) playCaptainRound(player, playerState, playerAction);
        else if (playerAction.action == Action.EXCHANGE_AS_AMBASSADOR) playAmbassadorRound(player, playerState, playerAction);
        else if (playerAction.action == Action.ASSASSINATE) playAssassinRound(player, playerState, playerAction);
        else if (playerAction.action == Action.COUP) playCoupRound(playerState, playerAction);
        else {
          System.out.println("Invalid action, should be unreachable block: " + playerAction.action);
          System.exit(1);
        }
      }
    }
  }

  private static void playIncomeRound(PlayerState playerState, Action playerAction) {
    playerState.coins += 1;
  }

  private static void playForeignAidRound(PlayerState playerState, Action playerAction) throws Exception {
    // ask for duke counteractions
    Action counterAction = null;

    List<Action> playersWhoBlockForeignAid = new ArrayList<Action>();
    List<Integer> otherPlayersIds = getOtherPlayers(playerState.playerId);
    for (Integer otherPlayerId : otherPlayersIds) {
      Player otherPlayer = getPlayerForId(otherPlayerId);
      PlayerState otherPlayerState = getPlayerStateForId(otherPlayerId);

      Action otherPlayerCounterAction = otherPlayer.doCounterAction(otherPlayerState.cards, otherPlayerState.coins, playerAction);
      if (otherPlayerCounterAction != null) {
        validateCounteraction(playerAction, otherPlayerState, otherPlayerCounterAction);
        playersWhoBlockForeignAid.add(otherPlayerCounterAction);
      }
    }
    // pick one at random
    if (playersWhoBlockForeignAid.size() > 0) {
      Random ran = new Random();
      int x = ran.nextInt(playersWhoBlockForeignAid.size());
      counterAction = playersWhoBlockForeignAid.get(x);
    }

    // if counterAction is not null, ask for BS. otherwise, there was no counteraction, and simply resolve.
    if (counterAction != null) {

      PlayerState playerStateWhoDidCounteraction = getPlayerStateForId(counterAction.playerDoingAction);
      Player playerWhoDidCounteraction = getPlayerForId(counterAction.playerDoingAction);

      System.out.println("Player: " + playerWhoDidCounteraction.playerId + " did counteraction: " + counterAction.action);

      // ask for BS
      Integer playerCallingBS = askingForBS(counterAction);

      if (playerCallingBS != null) {

        boolean playerHadCard =
            resolveBS(playerWhoDidCounteraction, playerStateWhoDidCounteraction, counterAction, playerCallingBS);

        if (playerHadCard) {
        } else { // original action should go through.
          System.out.println("Player: " + playerAction.playerDoingAction + " gets two coins");
          playerState.coins += 2;
          logPlayerStates();
          return;
        }
      }
    } else {
      System.out.println("Player: " + playerAction.playerDoingAction + " gets two coins");
      playerState.coins += 2;
      logPlayerStates();
      return;
    }
  }

  private static void playDukeRound(Player player, PlayerState playerState, Action playerAction) throws Exception {
    // ask for BS
    Integer playerCallingBS = askingForBS(playerAction);

    if (playerCallingBS != null) {
      boolean playerHadCard = resolveBS(player, playerState, playerAction, playerCallingBS);

      if (playerHadCard) { // simply resolve the action
        System.out.println("Player: " + playerAction.playerDoingAction + " gets three coins");
        playerState.coins += 3;
        logPlayerStates();
        return;
      } else {
        // nothing happens, player lost card for lying about duke, does not get coins, move on
        logPlayerStates();
        return;
      }
    } else { // if no one called BS, player simply gets the coins
      System.out.println("Player: " + playerAction.playerDoingAction + " gets three coins");
      playerState.coins += 3;
      logPlayerStates();
      return;
    }
  }

  private static void playCaptainRound(Player player, PlayerState playerState, Action playerAction) throws Exception {
    Player targetedPlayer = getPlayerForId(playerAction.targetPlayer);
    PlayerState targetedPlayerState = getPlayerStateForId(playerAction.targetPlayer);

    // ask for BS
    Integer playerCallingBS = askingForBS(playerAction);

    if (playerCallingBS != null) {
      boolean playerHadCard = resolveBS(player, playerState, playerAction, playerCallingBS);

      if (playerHadCard) { // round continues and person being stolen from can still block

        if (targetedPlayerState.cards.size() == 0) {
          return;
        }
        Action counterAction = targetedPlayer.doCounterAction(targetedPlayerState.cards, targetedPlayerState.coins, playerAction);
        if (counterAction != null) {
          validateCounteraction(playerAction, targetedPlayerState, counterAction);
        }
        if (counterAction != null) {
          System.out.println("Player: " + targetedPlayer.playerId + " did counteraction: " + counterAction.action);
          // ask for bs
          playerCallingBS = askingForBS(counterAction);

          if (playerCallingBS != null) {
            playerHadCard = resolveBS(targetedPlayer, targetedPlayerState, counterAction, playerCallingBS);

            if (playerHadCard) { // original action does not go through since counteraction player had the card
            } else { // original action should go through since player did not have counteraction card
              if (targetedPlayerState.coins > 1) {
                targetedPlayerState.coins -= 2;
                playerState.coins += 2;
                System.out.println("Player: " + playerAction.playerDoingAction + " stole 2 from " + playerAction.targetPlayer);
                logPlayerStates();
              } else if (targetedPlayerState.coins == 1) {
                targetedPlayerState.coins = 0;
                playerState.coins += 1;
                System.out.println("Player: " + playerAction.playerDoingAction + " stole 1 from " + playerAction.targetPlayer);
                logPlayerStates();
              } else { // player had no coins, weird that player chose to steal from him...
                System.out.println("Player: " + playerAction.playerDoingAction + " stole 0 from " + playerAction.targetPlayer);
                logPlayerStates();
              }
            }
          } else { // no one called BS on counteraction, so original action does not happen, turn over
            return;
          }
        } else {
          // coins are stolen
          if (targetedPlayerState.coins > 1) {
            targetedPlayerState.coins -= 2;
            playerState.coins += 2;
            System.out.println("Player: " + playerAction.playerDoingAction + " stole 2 from " + playerAction.targetPlayer);
            logPlayerStates();
          } else if (targetedPlayerState.coins == 1) {
            targetedPlayerState.coins = 0;
            playerState.coins += 1;
            System.out.println("Player: " + playerAction.playerDoingAction + " stole 1 from " + playerAction.targetPlayer);
            logPlayerStates();
          } else { // player had no coins, weird that player chose to steal from him...
            System.out.println("Player: " + playerAction.playerDoingAction + " stole 0 from " + playerAction.targetPlayer);
            logPlayerStates();
          }
        }
      }
    } else { // counter action is allowed now
      Action counterAction = targetedPlayer.doCounterAction(targetedPlayerState.cards, targetedPlayerState.coins, playerAction);
      if (counterAction != null) {
        validateCounteraction(playerAction, targetedPlayerState, counterAction);
      }
      if (counterAction != null) {
        System.out.println("Player: " + targetedPlayer.playerId + " did counteraction: " + counterAction.action);
        // ask for bs
        playerCallingBS = askingForBS(counterAction);

        if (playerCallingBS != null) {
          boolean playerHadCard = resolveBS(targetedPlayer, targetedPlayerState, counterAction, playerCallingBS);

          if (playerHadCard) { // original action does not go through since counteraction player had the card
          } else { // original action should go through since player did not have counteraction card
            if (targetedPlayerState.coins > 1) {
              targetedPlayerState.coins -= 2;
              playerState.coins += 2;
              System.out.println("Player: " + playerAction.playerDoingAction + " stole 2 from " + playerAction.targetPlayer);
              logPlayerStates();
            } else if (targetedPlayerState.coins == 1) {
              targetedPlayerState.coins = 0;
              playerState.coins += 1;
              System.out.println("Player: " + playerAction.playerDoingAction + " stole 1 from " + playerAction.targetPlayer);
              logPlayerStates();
            } else { // player had no coins, weird that player chose to steal from him...
              System.out.println("Player: " + playerAction.playerDoingAction + " stole 0 from " + playerAction.targetPlayer);
              logPlayerStates();
            }
          }
        } else { // no one called BS on counteraction, so original action does not happen, turn over
          return;
        }
      } else {
        // coins are stolen
        if (targetedPlayerState.coins > 1) {
          targetedPlayerState.coins -= 2;
          playerState.coins += 2;
          System.out.println("Player: " + playerAction.playerDoingAction + " stole 2 from " + playerAction.targetPlayer);
          logPlayerStates();
        } else if (targetedPlayerState.coins == 1) {
          targetedPlayerState.coins = 0;
          playerState.coins += 1;
          System.out.println("Player: " + playerAction.playerDoingAction + " stole 1 from " + playerAction.targetPlayer);
          logPlayerStates();
        } else { // player had no coins, weird that player chose to steal from him...
          System.out.println("Player: " + playerAction.playerDoingAction + " stole 0 from " + playerAction.targetPlayer);
          logPlayerStates();
        }
      }
    }
  }

  private static void playAmbassadorRound(Player player, PlayerState playerState, Action playerAction) throws Exception {
      Integer playerCallingBS = askingForBS(playerAction);

    if (playerCallingBS != null) {

      // resolve the BS
      boolean playerHadCard = resolveBS(player, playerState, playerAction, playerCallingBS);
      if (playerHadCard) { // do the ambassador action
        System.out.println("Player: " + playerAction.playerDoingAction + " does ambassador");

        List<String> cardsFromDeck = new ArrayList<String>();
        cardsFromDeck.add(deck.remove(0));
        cardsFromDeck.add(deck.remove(0));
        System.out.println("Cards from Deck: " + cardsFromDeck);
        List<String> returnedCards = player.resolveAmbassador(playerState.cards, playerState.coins, cardsFromDeck);
        deck.add(returnedCards.get(0));
        deck.add(returnedCards.get(0));
        Collections.shuffle(deck);

        // update playerState with new cards
        System.out.println("cards before ambassador: " + playerState.cards);
        playerState.cards.addAll(cardsFromDeck);
        System.out.println("cards with cards from deck: " + playerState.cards);

        for (String returnedCard : returnedCards) {
          playerState.cards.remove(returnedCard);
        }

        System.out.println("cards with removed returned cards: " + playerState.cards);
      } else { // do nothing
        return;
      }

    } else { // no one called BS so do ambassador action
      System.out.println("Player: " + playerAction.playerDoingAction + " does ambassador");

      List<String> cardsFromDeck = new ArrayList<String>();
      cardsFromDeck.add(deck.remove(0));
      cardsFromDeck.add(deck.remove(0));
      System.out.println("Cards from Deck: " + cardsFromDeck);
      List<String> returnedCards = player.resolveAmbassador(playerState.cards, playerState.coins, cardsFromDeck);
      deck.add(returnedCards.get(0));
      deck.add(returnedCards.get(0));
      Collections.shuffle(deck);

      // update playerState with new cards
      System.out.println("cards before ambassador: " + playerState.cards);
      playerState.cards.addAll(cardsFromDeck);
      System.out.println("cards with cards from deck: " + playerState.cards);

      for (String returnedCard : returnedCards) {
        playerState.cards.remove(returnedCard);
      }

      System.out.println("cards with removed returned cards: " + playerState.cards);
    }
  }

  private static void playAssassinRound(Player player, PlayerState playerState, Action playerAction) throws Exception {
    playerState.coins -= 3;

    Player targetedPlayer = getPlayerForId(playerAction.targetPlayer);
    PlayerState targetedPlayerState = getPlayerStateForId(playerAction.targetPlayer);

    // ask for BS
    Integer playerCallingBS = askingForBS(playerAction);

    if (playerCallingBS != null) {
      boolean playerHadCard = resolveBS(player, playerState, playerAction, playerCallingBS);

      if (playerHadCard) { // round continues and person being stolen from can still block

        if (targetedPlayerState.cards.size() == 0) {
          return;
        }
        Action counterAction = targetedPlayer.doCounterAction(targetedPlayerState.cards, targetedPlayerState.coins, playerAction);
        if (counterAction != null) {
          validateCounteraction(playerAction, targetedPlayerState, counterAction);
        }
        if (counterAction != null) {
          System.out.println("Player: " + targetedPlayer.playerId + " did counteraction: " + counterAction.action);
          // ask for bs
          playerCallingBS = askingForBS(counterAction);

          if (playerCallingBS != null) {
            playerHadCard = resolveBS(targetedPlayer, targetedPlayerState, counterAction, playerCallingBS);

            if (playerHadCard) { // original action does not go through since counteraction player had the card
            } else { // original action should go through since player did not have counteraction card
              if (targetedPlayerState.cards.size() == 2) {
                String lostCard = targetedPlayer.loseCard(targetedPlayerState.cards, targetedPlayerState.coins);
                targetedPlayerState.cards.remove(lostCard);
                revealedCards.add(lostCard);
                System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard);
                logPlayerStates();
              } else if (targetedPlayerState.cards.size() == 1) {
                String lostCard = targetedPlayerState.cards.remove(0);
                revealedCards.add(lostCard);
                System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard + " and is out");
                logPlayerStates();
              }
            }
          } else { // no one called BS on counteraction, so original action does not happen, turn over
            return;
          }
        } else {
          if (targetedPlayerState.cards.size() == 2) {
            String lostCard = targetedPlayer.loseCard(targetedPlayerState.cards, targetedPlayerState.coins);
            targetedPlayerState.cards.remove(lostCard);
            revealedCards.add(lostCard);
            System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard);
            logPlayerStates();
          } else if (targetedPlayerState.cards.size() == 1) {
            String lostCard = targetedPlayerState.cards.remove(0);
            revealedCards.add(lostCard);
            System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard + " and is out");
            logPlayerStates();
          }
        }
      }
    } else { // counter action is allowed now
      Action counterAction = targetedPlayer.doCounterAction(targetedPlayerState.cards, targetedPlayerState.coins, playerAction);
      if (counterAction != null) {
        validateCounteraction(playerAction, targetedPlayerState, counterAction);
      }
      if (counterAction != null) {
        System.out.println("Player: " + targetedPlayer.playerId + " did counteraction: " + counterAction.action);

        // ask for bs
        playerCallingBS = askingForBS(counterAction);

        if (playerCallingBS != null) {
          boolean playerHadCard = resolveBS(targetedPlayer, targetedPlayerState, counterAction, playerCallingBS);

          if (playerHadCard) { // original action does not go through since counteraction player had the card
          } else { // original action should go through since player did not have counteraction card
            if (targetedPlayerState.cards.size() == 2) {
              String lostCard = targetedPlayer.loseCard(targetedPlayerState.cards, targetedPlayerState.coins);
              targetedPlayerState.cards.remove(lostCard);
              revealedCards.add(lostCard);
              System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard);
              logPlayerStates();
            } else if (targetedPlayerState.cards.size() == 1) {
              String lostCard = targetedPlayerState.cards.remove(0);
              revealedCards.add(lostCard);
              System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard + " and is out");
              logPlayerStates();
            }
          }
        } else { // no one called BS on counteraction, so original action does not happen, turn over
          return;
        }
      } else {
        if (targetedPlayerState.cards.size() == 2) {
          String lostCard = targetedPlayer.loseCard(targetedPlayerState.cards, targetedPlayerState.coins);
          targetedPlayerState.cards.remove(lostCard);
          revealedCards.add(lostCard);
          System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard);
          logPlayerStates();
        } else if (targetedPlayerState.cards.size() == 1) {
          String lostCard = targetedPlayerState.cards.remove(0);
          revealedCards.add(lostCard);
          System.out.println("Player: " + targetedPlayerState.playerId + " lost card: " + lostCard + " and is out");
          logPlayerStates();
        }
      }
    }
  }

  private static void playCoupRound(PlayerState playerState, Action playerAction) throws Exception {
    playerState.coins -= 7;
    // get targeted player
    Player targetedPlayer = getPlayerForId(playerAction.targetPlayer);
    PlayerState targetedPlayerState = getPlayerStateForId(playerAction.targetPlayer);

    if (targetedPlayerState.cards.size() == 2) {
      String lostCard = targetedPlayer.loseCard(targetedPlayerState.cards, targetedPlayerState.coins);
      targetedPlayerState.cards.remove(lostCard);
      revealedCards.add(lostCard);
      System.out.println("Player: " + targetedPlayerState.playerId + " lost card " + lostCard);
      logPlayerStates();
    } else { // targeted player has 1 card
      String lostCard = targetedPlayerState.cards.remove(0);
      revealedCards.add(lostCard);
      System.out.println("Player: " + targetedPlayerState.playerId + " lost card " + lostCard + " and is out");
      logPlayerStates();
    }
  }

  // returns true if action is valid, meaning player had card. use that boolean to know whether to have action go through or not
  private static boolean resolveBS(Player player, PlayerState playerState, Action playerAction, Integer playerCallingBS) throws Exception {
    // resolve the BS
    String playersCard = getCardThatHasAction(playerState.cards, playerAction);

    if (playersCard == null) { // player did not have action card
      System.out.println("Player: " + playerState.playerId + " did not have card for action: " + playerAction.action);
      if (playerState.cards.size() == 2) {
        String lostCard = player.loseCard(playerState.cards, playerState.coins);
        playerState.cards.remove(lostCard);
        revealedCards.add(lostCard);
        System.out.println("Player: " + playerState.playerId + " lost card: " + lostCard);
        logPlayerStates();
      } else { // player has 1 card
        String lostCard = playerState.cards.remove(0);
        revealedCards.add(lostCard);
        System.out.println("Player: " + playerState.playerId + " lost card: " + lostCard + " and is out");
        logPlayerStates();
      }
      return false;
    } else { // person who did BS loses card, original action does not go through, player who did action gets new card
      PlayerState playerStateWhoCalledBS = getPlayerStateForId(playerCallingBS);
      Player playerWhoCalledBS = getPlayerForId(playerCallingBS);

      if (playerStateWhoCalledBS.cards.size() == 2) {
        String lostCard = playerWhoCalledBS.loseCard(playerStateWhoCalledBS.cards, playerStateWhoCalledBS.coins);
        playerStateWhoCalledBS.cards.remove(lostCard);
        revealedCards.add(lostCard);
        System.out.println("Player: " + playerStateWhoCalledBS.playerId + " lost card: " + lostCard);
        logPlayerStates();
      } else { // player has 1 card
        String lostCard = playerStateWhoCalledBS.cards.remove(0);
        revealedCards.add(lostCard);
        System.out.println("Player: " + playerStateWhoCalledBS.playerId + " lost card: " + lostCard + " and is out");
        logPlayerStates();
      }

      // person who did counteraction needs to swap out for a new card
      playerState.cards.remove(playersCard);
      deck.add(playersCard);
      Collections.shuffle(deck);
      System.out.println("deck after adding playersCard: " + playersCard + " and shuffling is: " + deck);
      String newCard = deck.remove(0);
      playerState.cards.add(newCard);
      System.out.println("Player: " + playerState.playerId + " got new card: " + newCard);
      logPlayerStates();
      return true;
    }
  }

  private static void validateAction(PlayerState playerState, Action playerAction) {
    boolean valid = false;
    Set<String> actions = new HashSet<String>();
    actions.add(Action.TAKE_INCOME);
    actions.add(Action.TAKE_FOREIGN_AID);
    actions.add(Action.TAKE_3_AS_DUKE);
    actions.add(Action.STEAL_2_AS_CAPTAIN);
    actions.add(Action.EXCHANGE_AS_AMBASSADOR);

    if (actions.contains(playerAction.action)) {
      valid = (playerState.coins < 10 && playerState.playerId == playerAction.playerDoingAction);
    }

    else if (playerAction.action == Action.ASSASSINATE) {
      valid = (playerState.coins >= 3 && playerState.playerId == playerAction.playerDoingAction && playerState.playerId != playerAction.targetPlayer);
    }

    else if (playerAction.action == Action.COUP) {
      valid = (playerState.coins >= 7 && playerState.playerId == playerAction.playerDoingAction && playerState.playerId != playerAction.targetPlayer);
    }
    if (!valid) {
      System.out.println("Player: " + playerState.playerId + " did illegal action: " + playerAction + ", exiting");
      logPlayerStates();
      System.exit(1);
    }
  }

  private static void validateCounteraction(Action playerAction, PlayerState counterPlayerState, Action counterAction) throws Exception {
    boolean valid = false;
    if (playerAction.action == Action.TAKE_FOREIGN_AID) {
      valid = (counterAction.action.equals(Action.BLOCK_FOREIGN_AID) && counterPlayerState.playerId == counterAction.playerDoingAction);
    }
    else if (playerAction.action == Action.STEAL_2_AS_CAPTAIN) {
      valid = (counterAction.action.equals(Action.BLOCK_STEAL_AS_CAPTAIN)
          || (counterAction.action.equals(Action.BLOCK_STEAL_AS_AMBASSADOR))
          && counterPlayerState.playerId == counterAction.playerDoingAction);
    }
    else if (playerAction.action == Action.ASSASSINATE) {
      valid = (counterAction.action.equals(Action.BLOCK_ASSASSINATION) && counterPlayerState.playerId == counterAction.playerDoingAction);
    }

    if (!valid) {
      System.out.println("Player: " + counterPlayerState.playerId + " did illegal counterAction: " + playerAction + ", exiting");
      logPlayerStates();
      System.exit(1);
    }
  }

  private static Integer askingForBS(Action action) throws Exception {
    // return one at random if multiple people BS, null if no one BS
    List<Integer> otherPlayersIds = getOtherPlayers(action.playerDoingAction);

    List<Integer> playersWhoCallBS = new ArrayList<Integer>();
    for (Integer otherPlayerId : otherPlayersIds) {
      Player otherPlayer = getPlayerForId(otherPlayerId);
      PlayerState otherPlayerState = getPlayerStateForId(otherPlayerId);
      if (otherPlayer.doBS(otherPlayerState.cards, otherPlayerState.coins, action)) {
        playersWhoCallBS.add(otherPlayerState.playerId);
      }
    }

    if (playersWhoCallBS.size() > 0) {
      Random ran = new Random();
      int x = ran.nextInt(playersWhoCallBS.size());
      int playerIdWhoCalledBS = playersWhoCallBS.get(x);
      System.out.println("Player: " + playerIdWhoCalledBS + " called BS");
      return playerIdWhoCalledBS;
    } else {
      System.out.println("No one called BS");
      return null;
    }
  }

  private static void logPlayerStates() {
    System.out.println("\n");
    for (PlayerState ps : players) {
      System.out.println("playerId: " + ps.playerId + " has cards: " + ps.cards + " and coins: " + ps.coins);
    }
    System.out.println("\n");
  }

  private static boolean checkIfGameIsDone() {
    int numPlayersWithOneOrMoreCards = 0;
    for (PlayerState ps : players) {
      if (ps.cards.size() > 0) {
        numPlayersWithOneOrMoreCards += 1;
      }
    }
    return !(numPlayersWithOneOrMoreCards > 1);
  }

  private static void logWinner() {
    for (PlayerState ps : players) {
      if (ps.cards.size() > 0) {
        System.out.println("Congratulations to playerId: " + ps.playerId);
      }
    }
  }

  private static void logAction(Action action) {
    if (action.targetPlayer == null) {
      System.out.println("Player : " + action.playerDoingAction + " did: " + action.action);
    } else {
      System.out.println("Player : " + action.playerDoingAction + " did: " + action.action + " against: " + action.targetPlayer);
    }
  }

  private static String getCardThatHasAction(List<String> cards, Action action) throws Exception {
    if (action.action == Action.TAKE_3_AS_DUKE || action.action == Action.BLOCK_FOREIGN_AID) {
      for (String card : cards) {
        if (card == Card.DUKE) return Card.DUKE;
      }
      return null;
    } else if (action.action == Action.STEAL_2_AS_CAPTAIN || action.action == Action.BLOCK_STEAL_AS_CAPTAIN) {
      for (String card : cards) {
        if (card == Card.CAPTAIN) return Card.CAPTAIN;
      }
      return null;
    } else if (action.action == Action.EXCHANGE_AS_AMBASSADOR || action.action == Action.BLOCK_STEAL_AS_AMBASSADOR) {
      for (String card : cards) {
        if (card == Card.AMBASSADOR) return Card.AMBASSADOR;
      }
      return null;
    } else if (action.action == Action.ASSASSINATE) {
      for (String card : cards) {
        if (card == Card.ASSASSIN) return Card.ASSASSIN;
      }
      return null;
    } else if (action.action == Action.BLOCK_ASSASSINATION) {
      for (String card : cards) {
        if (card == Card.CONTESSA) return Card.CONTESSA;
      }
      return null;
    }
    throw new Exception("Invalid action: " + action.action);
  }

  private static PlayerState getPlayerStateForId(int playerId) throws Exception {
    for (PlayerState ps: players) {
      if (ps.playerId == playerId) {
        return ps;
      }
    }
    throw new Exception("Could not find playerId: " + playerId);
  }

  private static Player getPlayerForId(int playerId) throws Exception {
    for (Player p : AIs) {
      if (p.playerId == playerId) return p;
    }
    throw new Exception("Could not find player");
  }


  // APIs available to users to get game state information
  public static List<Integer> getOtherPlayers(int playerId) {
    List<Integer> retval = new ArrayList<Integer>();
    for (PlayerState ps : players) {
      if (ps.playerId != playerId && ps.cards.size() > 0) {
        retval.add(ps.playerId);
      }
    }
    return retval;
  }

  public static int getNumCardsForPlayer(int playerId) throws Exception {
    PlayerState player = getPlayerStateForId(playerId);
    return player.cards.size();
  }

  public static int getNumCoinsForPlayer(int playerId) throws Exception {
    PlayerState player = getPlayerStateForId(playerId);
    return player.coins;
  }

  public static List<String> getRevealedCards() {
    return revealedCards;
  }

  /* TODO, API that gets all past actions/counteractions/BS calls for each player, in order
  need to think about what the return type for this should be... custom object?
  public List<Action> getGameHistory() {

  }
  */
}
