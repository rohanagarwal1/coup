package personal.rsa.game;

import java.util.List;


public class PlayerState {
  public int coins;
  public List<String> cards;
  public int playerId;

  public PlayerState(List<String> cards, int playerId) {
    this.cards = cards;
    this.playerId = playerId;
    this.coins = 2;
  }
}
