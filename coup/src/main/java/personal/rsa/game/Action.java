package personal.rsa.game;

public class Action {

  public static final String TAKE_INCOME = "TAKE_INCOME";
  public static final String TAKE_FOREIGN_AID = "TAKE_FOREIGN_AID";
  public static final String TAKE_3_AS_DUKE = "TAKE_3_AS_DUKE";
  public static final String STEAL_2_AS_CAPTAIN = "STEAL_2_AS_CAPTAIN";
  public static final String EXCHANGE_AS_AMBASSADOR = "EXCHANGE_AS_AMBASSADOR";
  public static final String ASSASSINATE = "ASSASSINATE";
  public static final String COUP = "COUP";

  public static final String BLOCK_FOREIGN_AID = "BLOCK_FOREIGN_AID";
  public static final String BLOCK_STEAL_AS_CAPTAIN = "BLOCK_STEAL_AS_CAPTAIN";
  public static final String BLOCK_STEAL_AS_AMBASSADOR = "BLOCK_STEAL_AS_AMBASSADOR";
  public static final String BLOCK_ASSASSINATION = "BLOCK_ASSASSINATION";

  public Integer playerDoingAction;
  public String action;
  public Integer targetPlayer;

  public Action(Integer playerDoingAction, String action, Integer targetPlayer) {
    this.playerDoingAction = playerDoingAction;
    this.action = action;
    this.targetPlayer = targetPlayer;
  }

  public Action(Integer playerDoingAction, String action) {
    this(playerDoingAction, action, null);
  }

  // TODO: Implement
  public String toString() {
    return "";
  }
}

