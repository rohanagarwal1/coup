This project makes it possible to build AIs to play Coup.

https://boardgamegeek.com/boardgame/131357/coup

From the root of the project, run `./gradlew clean build` to produce a jar.

Run the jar using `java -jar coup/build/libs/coup.jar`
